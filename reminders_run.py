import handlers.SQLite3 as SQLite3
import asyncio
from datetime import datetime
import handlers.config as config

from aiogram import Bot

db=SQLite3.SQLite3(config.name_db)
bot=Bot(config.BOT_TOKEN)

async def check():
    while True:
        reminders_results = db.get_all_reminders()
        #
        day = datetime.today().strftime("%d/%m/%Y")
        time = datetime.now().strftime("%H:%M")
        for x in reminders_results:
            if x[3] == day:
                if x[4] == time:
                        db.delete_reminder(x[0])
                        db.commit()
                        await bot.send_message(chat_id=x[1], text="Напоминание:\n"+x[2])
        #
        await asyncio.sleep(60)
        
async def start():
    current_sec=int(datetime.now().strftime("%S"))
    delay=60-current_sec
    if delay == 60:
        delay=0

    await asyncio.sleep(delay)
    await check()