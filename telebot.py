import asyncio
import logging
import handlers.SQLite3 as SQLite3

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

import handlers.config as config 
from handlers.start_help import register_handlers_common
from handlers.delete_reminder import register_handlers_del_reminder
from handlers.edit_reminders import register_handlers_edit_reminder
from handlers.all_reminders import register_handlers_my_reminders
from handlers.set_reminder import register_handlers_set_reminder
from handlers.UTC import register_handlers_utc
from datetime import datetime
import reminders_run

async def main(loop):
    
    bot = Bot(token=config.BOT_TOKEN)
    dp = Dispatcher(bot, storage=MemoryStorage())
    db=SQLite3.SQLite3(config.name_db)
    db.commit()
    
    register_handlers_common(dp)
    register_handlers_utc(dp)
    register_handlers_set_reminder(dp)
    register_handlers_my_reminders(dp)
    register_handlers_del_reminder(dp)
    register_handlers_edit_reminder(dp)
    asyncio.ensure_future(reminders_run.start())
    #
    await dp.start_polling()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))