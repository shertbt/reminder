from aiogram import types
from aiogram.utils.callback_data import CallbackData
from aiogram.types import CallbackQuery
import datetime
import calendar
import json

def UTC_keyboard():
    keyboard=types.InlineKeyboardMarkup()
    names=['UTC+2','UTC+3','UTC+4','UTC+5','UTC+6','UTC+7','UTC+8',
           'UTC+9','UTC+10','UTC+11','UTC+12']
    buttons=[types.InlineKeyboardButton(text='🕒'+x,callback_data=x)for x in names]
    keyboard.add(*buttons)
    return keyboard
