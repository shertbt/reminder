import handlers.SQLite3 as SQLite3
from aiogram_calendar import simple_cal_callback, SimpleCalendar
from aiogram import Dispatcher, types, Bot
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import MessageNotModified
import handlers.config as config
import keyboards

db = SQLite3.SQLite3(config.name_db)
bot = Bot(token=config.BOT_TOKEN)

class UTC(StatesGroup):
    start = State()
    waiting_for_time_zone = State()



async def get_utc(message: types.Message):
    markup=keyboards.UTC_keyboard()
    await message.reply('Выберите часовой пояс',reply_markup=markup,parse_mode=types.ParseMode.HTML)
    await UTC.waiting_for_time_zone.set()

async def callback_UTC(callback: types.CallbackQuery):
    await callback.message.edit_text(text=callback.message.text)
    UTC=str(callback.data)
    db.change_UTC(UTC,callback.from_user.id)
    db.commit()
    await callback.message.answer(f'Вы выбрали часовой пояс {UTC}')
    await UTC.first()
    await callback.answer()

def register_handlers_utc(dp: Dispatcher):
    dp.register_message_handler(get_utc, commands="utc", state="*")
    dp.register_callback_query_handler(callback_UTC, state=UTC.waiting_for_time_zone)