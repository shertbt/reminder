import handlers.SQLite3 as SQLite3
from aiogram import Dispatcher, types, Bot
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import MessageNotModified
import handlers.config as config
import datetime

db = SQLite3.SQLite3(config.name_db)

async def view_all(message):
    reminders=db.get_user_reminders(message.from_user.id)
    if reminders:
        
        for x in reminders:
            #
            time = 'Время: ' + x[4]
            #
            datetime_object = datetime.datetime.strptime(x[3],"%d/%m/%Y")
            data = 'Дата: ' +  datetime_object.strftime("%d/%m/%Y")
            
            name = 'Название: ' +x[2]
            #
            text =  time + '\n' + data + '\n' + name
            await message.answer(text=text)
    #
    else:
        await message.answer("Нет напоминаний")

def register_handlers_my_reminders(dp: Dispatcher):
    dp.register_message_handler(view_all, commands="all", state='*')