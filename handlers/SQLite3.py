import sqlite3

class SQLite3:
    def __init__(self, database):
        """Подключние к БД и сохранение курсора соединения"""
        self.connection = sqlite3.connect(database, check_same_thread=False)
        self.cursor = self.connection.cursor()

        with self.connection:
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS "users"(
                                "user_chat_id"	INTEGER,
                                "username"	TEXT,
                                "first_name"	TEXT,
                                "last_name"	TEXT,
                                "UTC"    TEXT,
                                PRIMARY KEY("user_chat_id")
                                )''')
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS "reminders" (
                                "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
                                "user_chat_id"	INTEGER,
                                "name"	TEXT,
                                "date"	TEXT,
                                "time"	TEXT
                                )''')
    def commit(self):
        self.connection.commit()
    
    def close(self):
        self.connection.close()
  
    def check_user(self,message):
        chat_id: str = message.from_user.id
        find_user_query: str = "select username from users where user_chat_id='{}'".format(chat_id)
        self.cursor.execute(find_user_query)
        user_found = self.cursor.fetchall()
        return user_found
    
    def check_id(self,id):
        find_user_query: str = "select username from users where user_chat_id='{}'".format(id)
        self.cursor.execute(find_user_query)
        user_found = self.cursor.fetchall()
        return user_found
    
    def insert_user_to_db(self, user):
        
        # Проверяет, есть ли пользователь в БД
        user_found=self.check_id(user[0])

        # Добавляем пользователя в БД, если его в ней нет
        if not user_found:
            insert_query: str = "insert into users (user_chat_id,username, first_name, last_name, UTC) values ('{}', '{}', " \
                                "'{}', '{}','{}')".format(
                user[0], user[1], user[2], user[3],user[4])
            self.cursor.execute(insert_query)
        else:
            pass

    def change_UTC(self,UTC,chat_id):
         # SQL query для удаления места из БД
        edit_query: str = "update users set UTC='{}' where " \
                          "user_chat_id='{}'" \
            .format(UTC, chat_id)
        self.cursor.execute(edit_query)

    def new_reminder(self,reminder):
        chat_id=reminder[0]
        name=str(reminder[1])
        date=str(reminder[2])
        time=str(reminder[3])
        
        insert_query: str = "insert into reminders (user_chat_id,name,date,time) values ('{}','{}','{}','{}')".format(chat_id,name,date,time)
        self.cursor.execute(insert_query)
    
    def get_user_reminders(self,chat_id):
        select_query: str = "select * from reminders where "\
                            "user_chat_id='{}'"\
                            .format(chat_id)
        
        self.cursor.execute(select_query)
        reminders = self.cursor.fetchall()
        return reminders
    
    def change_time(self,reminder_id,time):
        edit_query: str = "update reminders set time='{}' where " \
                          "id='{}'" \
            .format(time,reminder_id)
        self.cursor.execute(edit_query)

    def change_name(self,reminder_id,new_name):
        edit_query: str = "update reminders set name='{}' where " \
                          "id='{}'" \
            .format(new_name,reminder_id)
        self.cursor.execute(edit_query)

    def change_date(self,r_id,new_data):
        edit_query: str = "update reminders set date='{}' where " \
                          "id='{}'" \
            .format(new_data,r_id)
        self.cursor.execute(edit_query)
    

    def delete_reminder(self,reminder_id):
        delete_query: str = "delete from reminders where id = '{}'".format(reminder_id)
        self.cursor.execute(delete_query)

    def get_all_reminders(self):
        select_query: str = "select * from reminders"
        
        self.cursor.execute(select_query)
        reminders = self.cursor.fetchall()
        return reminders
    
    
    