import handlers.SQLite3 as SQLite3
from aiogram import Dispatcher, types, Bot
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import MessageNotModified
import handlers.config as config
import datetime

db = SQLite3.SQLite3(config.name_db)
bot=Bot(config.BOT_TOKEN)

class DelReminder(StatesGroup):
    start = State()
    waiting_for_reminder = State()

async def delete_reminder(message: types.Message):

    reminders=db.get_user_reminders(message.from_user.id)
    if reminders:
        
        keyboard = types.InlineKeyboardMarkup(row_width=2)
        keyboard.add(types.InlineKeyboardButton(text='◀️' +"Отменить", callback_data='cancel'))
        await message.reply("Выберите какое напоминание удалить", reply_markup=keyboard)
        #
        for x in reminders:
            keyboard_edit = types.InlineKeyboardMarkup(row_width=2)
            #
            time = 'Время: ' + x[4]
            #
            datetime_object = datetime.datetime.strptime(x[3],"%d/%m/%Y")
            data = 'Дата: ' +  datetime_object.strftime("%d/%m/%Y")
            
            name = 'Название: ' +x[2]
            #
            text =  time + '\n' + data + '\n' + name
            keyboard_edit.add(types.InlineKeyboardButton(text='Удалить',
                                                         callback_data=str(x[0])))
            
            await message.answer(text, reply_markup=keyboard_edit)
    #   
        await DelReminder.waiting_for_reminder.set()
    else:
        await message.answer("Нет напоминаний")

async def callback_delete(callback: types.CallbackQuery):
    reminders_results=db.get_user_reminders(callback.from_user.id)
    if callback.data!='cancel':
        reminder_id=int(callback.data)
        db.delete_reminder(reminder_id)
        db.commit()
        
        await bot.send_message(callback.message.chat.id,"Напоминание было удалено")
    else:
         #reminders_results=db.get_user_reminders(callback.from_user.id)
         for x in range(len(reminders_results)):
            await bot.delete_message(chat_id=callback.message.chat.id, message_id=callback.message.message_id + x + 1)
    
    await DelReminder.first()
    await callback.answer()


def register_handlers_del_reminder(dp: Dispatcher):
    dp.register_message_handler(delete_reminder, commands="delete", state="*")
    dp.register_callback_query_handler(callback_delete, state=DelReminder.waiting_for_reminder)