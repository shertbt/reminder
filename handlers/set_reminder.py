import handlers.SQLite3 as SQLite3
from aiogram_calendar import simple_cal_callback, SimpleCalendar
from aiogram import Dispatcher, types, Bot
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import MessageNotModified
import handlers.config as config
import datetime
db = SQLite3.SQLite3(config.name_db)
bot = Bot(token=config.BOT_TOKEN)

# Создаем список для напоминания 
reminder = [None] * 4

class SetReminder(StatesGroup):
    start = State()
    waiting_for_text = State()
    waiting_for_date = State()
    waiting_for_time = State()


async def new_notification(message: types.Message):
    await message.answer('📍 Введите название напоминания 📍')
    await SetReminder.waiting_for_text.set()

async def set_notification(message: types.Message):
    reminder[0]=message.from_user.id
    reminder[1] = message.text
    await message.answer("Выберите дату", reply_markup=await SimpleCalendar().start_calendar())
    await SetReminder.next()


async def process_simple_calendar(callback_query: types.CallbackQuery, callback_data: dict):
    selected, date = await SimpleCalendar().process_selection(callback_query, callback_data)
    if selected:
        if date.strftime("%d/%m/%Y")>= datetime.datetime.today().strftime("%d/%m/%Y"):
            reminder[2]=date.strftime("%d/%m/%Y")
            await callback_query.message.answer(
                f'Вы выбрали  {date.strftime("%d/%m/%Y")}')
            await SetReminder.next()
            await callback_query.message.answer('⏰ Введите время ⏰')
        else:
            await callback_query.message.answer(text="Неверный формат,попробуйте еще раз", reply_markup=await SimpleCalendar().start_calendar())


async def set_time(message: types.Message):
    try:
        if len(message.text) == 5 and int(message.text[0]) < 3 and int(message.text[1]) < 10 and \
                message.text[2] == ':' and int(message.text[3]) < 6 and int(message.text[4]) < 10 and \
                int(message.text[0:2]) < 24:
            reminder[3]=message.text
            db.new_reminder(reminder)
            db.commit()
            await message.answer(f'Вы выбрали {message.text}')
            await message.answer('Напоминание создано')
            await SetReminder.first()
        else:
            await message.answer(text="Неверный формат,попробуйте еще раз")
    except ValueError:
        await message.answer(text="Неверный форматб попробуйте еще раз")


def register_handlers_set_reminder(dp: Dispatcher):
    dp.register_message_handler(new_notification, commands="set", state="*")
    dp.register_message_handler(set_notification, state=SetReminder.waiting_for_text)
    dp.register_callback_query_handler(process_simple_calendar,simple_cal_callback.filter(),state=SetReminder.waiting_for_date)
    dp.register_message_handler(set_time, state=SetReminder.waiting_for_time)