import handlers.SQLite3 as SQLite3
from aiogram_calendar import simple_cal_callback, SimpleCalendar
from aiogram import Dispatcher, types, Bot
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import MessageNotModified
import handlers.config as config
import datetime

db = SQLite3.SQLite3(config.name_db)
bot = Bot(token=config.BOT_TOKEN)

rem_id=None

class EditReminder(StatesGroup):
    start = State()
    waiting_for_reminder = State()
    waiting_for_edit_type = State()
    editing_time = State()
    editing_days = State()
    editing_text = State()


async def edit_notification(message: types.Message):
    reminders=db.get_user_reminders(message.from_user.id)
    if reminders:
        
        keyboard = types.InlineKeyboardMarkup(row_width=2)
        keyboard.add(types.InlineKeyboardButton(text='◀️' +"Отменить", callback_data='cancel'))
        await message.reply("Выберите какое напоминание изменить", reply_markup=keyboard)
        #
        for x in reminders:
            keyboard_edit = types.InlineKeyboardMarkup(row_width=2)
            #
            time = 'Время: ' + x[4]
            #
            datetime_object = datetime.datetime.strptime(x[3],"%d/%m/%Y")
            data = 'Дата: ' +  datetime_object.strftime("%d/%m/%Y")
            
            name = 'Название: ' +x[2]
            #
            text =  time + '\n' + data + '\n' + name
            keyboard_edit.add(types.InlineKeyboardButton(text='Изменить',
                                                         callback_data=str(x[0])))
            await message.answer(text, reply_markup=keyboard_edit)
            
    #   
        await EditReminder.waiting_for_reminder.set()

    else:
        await message.answer("Нет напоминаний")


async def callback_edit(callback: types.CallbackQuery):
    
    reminders=db.get_user_reminders(callback.from_user.id)
    if callback.data=='cancel':
        for x in range(len(reminders)):
            await bot.delete_message(callback.message.chat.id, callback.message.message_id + x + 1)
    else:
        num=int(callback.data)
        global rem_id
        rem_id=num
        keyboard = types.InlineKeyboardMarkup(row_width=3)
        keyboard.add(types.InlineKeyboardButton(text='🕔' +' Время', callback_data="time"),
                     types.InlineKeyboardButton(text='📅' + "Дата", callback_data="date"),
                     types.InlineKeyboardButton(text='💬' + "Название", callback_data="name"))
        
    

        await callback.message.edit_text("Выберите изменение",reply_markup=keyboard)
    
        await EditReminder.next()

async def edit_type_choose(callback: types.CallbackQuery):
    if callback.data=='time':
        message = await callback.message.answer("Введите новое время")
        await EditReminder.editing_time.set()

    if callback.data=='name':
        message = await callback.message.answer("Введите новое название")
        await EditReminder.editing_text.set()

    if callback.data=='date':
        message = await callback.message.answer("Выберите новую дату", reply_markup=await SimpleCalendar().start_calendar())
        await EditReminder.editing_days.set()


async def change_time(message :types.Message):
    try:
        if len(message.text) == 5 and int(message.text[0]) < 3 and int(message.text[1]) < 10 and \
                message.text[2] == ':' and int(message.text[3]) < 6 and int(message.text[4]) < 10 and \
                int(message.text[0:2]) < 24:
            global rem_id
            new_time=message.text
            db.change_time(rem_id,new_time)
            db.commit()
            rem_id=None
            await bot.send_message(message.chat.id,"Время было изменено")
            await EditReminder.first()
        else:
            await message.answer(text="Неверный формат,попробуйте еще раз")
    except ValueError:
        await message.answer(text="Неверный формат,попробуйте еще раз")
        


async def change_name(message: types.Message):
    global rem_id
    new_name=message.text
    db.change_name(rem_id,new_name)
    db.commit()
    rem_id=None
    await bot.send_message(message.chat.id,"Название было изменено")   
    await EditReminder.first()


async def process_simple_calendar(callback_query: types.CallbackQuery, callback_data: dict):
    selected, date = await SimpleCalendar().process_selection(callback_query, callback_data)
    if selected:
        if date.strftime("%d/%m/%Y")>= datetime.datetime.today().strftime("%d/%m/%Y"):
            global rem_id
            await callback_query.message.answer(
                f'Вы выбрали  {date.strftime("%d/%m/%Y")}')
            db.change_date(rem_id,date.strftime("%d/%m/%Y"))
            db.commit()
            rem_id=None
            await EditReminder.first()
        else:
            await callback_query.message.answer(text="Неверный формат,попробуйте еще раз",reply_markup=await SimpleCalendar().start_calendar())

def register_handlers_edit_reminder(dp: Dispatcher):
    dp.register_message_handler(edit_notification, commands="edit", state="*")
    dp.register_callback_query_handler(callback_edit, state=EditReminder.waiting_for_reminder)
    dp.register_callback_query_handler(edit_type_choose, state=EditReminder.waiting_for_edit_type)
    dp.register_message_handler(change_time, state=EditReminder.editing_time)
    dp.register_callback_query_handler(process_simple_calendar,simple_cal_callback.filter(), state=EditReminder.editing_days)
    dp.register_message_handler(change_name, state=EditReminder.editing_text)
