import handlers.SQLite3 as SQLite3
from aiogram_calendar import simple_cal_callback, SimpleCalendar
from aiogram import Dispatcher, types, Bot
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import MessageNotModified
import handlers.config as config
import keyboards

db = SQLite3.SQLite3(config.name_db)

user=[None]*5

class Start(StatesGroup):
    start = State()
    waiting_for_time_zone = State()


async def get_help(message: types.Message):
    help = '''/set - создать напоминание\n/all - просмотреть все напоминания\n/delete - удалить напоминание\n/edit - изменить\n/help - просмотреть список команд'''
    await message.answer(help)


async def start_main(message: types.Message):
    user_found = db.check_user(message)
    await message.answer(f'Добро пожаловать,{message.from_user.first_name} {message.from_user.last_name}!')
    if not user_found:
        user[1]: str = message.from_user.username
        user[2]: str = message.from_user.first_name
        user[3]: str = message.from_user.last_name
        user[0]: int = message.from_user.id
        markup=keyboards.UTC_keyboard()
        await message.reply('Выберите часовой пояс',reply_markup=markup,parse_mode=types.ParseMode.HTML)
        await Start.waiting_for_time_zone.set()
        
async def callback_UTC(callback: types.CallbackQuery):
    await callback.message.edit_text(text=callback.message.text,parse_mode=types.ParseMode.HTML)
    
    UTC=str(callback.data)
    user[4]=UTC
    db.insert_user_to_db(user)
    db.commit()
    await callback.message.answer(f'Вы выбрали часовой пояс {UTC}')
    await Start.first()
    await callback.answer()

def register_handlers_common(dp: Dispatcher):
    dp.register_message_handler(start_main, commands="start", state="*")
    dp.register_callback_query_handler(callback_UTC, state=Start.waiting_for_time_zone)
    dp.register_message_handler(get_help, commands="help", state="*")